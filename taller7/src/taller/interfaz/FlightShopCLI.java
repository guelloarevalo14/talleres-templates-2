package taller.interfaz;

/*
 * FlightShopCLI.java
 * This file is part of FlightShopCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * FlightShop is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * FlightShop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FlightShop. If not, see <http:
 */

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Airline;
import taller.mundo.Airport;
import taller.mundo.Flight;
import taller.mundo.FlightShop;
import taller.mundo.Stop;

public class FlightShopCLI
{
     private Scanner in;
     private FlightShop flightShop;

     public FlightShopCLI()
     {
            in = new Scanner(System.in);
            System.out.println("Cargando información del sistema...");
            try
            {
                 flightShop = new FlightShop();
            }
            catch(Exception e)
            {
                 e.printStackTrace();
                 System.exit(-1);
            }
     }

     public void mainMenu() throws Exception
     {
            boolean finish = false;
            while(!finish)
            {
                  Screen.clear();
                  System.out.println("                          ....'....                          ");
                  Thread.sleep(100);
                  System.out.println("                     .;clooooooooooolc;.                     ");
                  Thread.sleep(100);
                  System.out.println("                  'coooooooooooooooooooooc'                  ");
                  Thread.sleep(100);
                  System.out.println("                ;looooooooooooooooooo:,:oool;                ");
                  Thread.sleep(100);
                  System.out.println("              'loooooooooooooooooool.   ;ooool'              ");
                  Thread.sleep(100);
                  System.out.println("             :oooooooooooooooooooo:    ,loooooo:             ");
                  Thread.sleep(100);
                  System.out.println("            cooooooooo:;lool:lool'   .coooooooooc            ");
                  Thread.sleep(100);
                  System.out.println("           ;oooo:'',,.  ';;. .:;.   .looooooooooo;           ");
                  Thread.sleep(100);
                  System.out.println("          .loool..                 ;oooooooooooool.          ");
                  Thread.sleep(100);
                  System.out.println("          'ooooooolc:,'..          .,;oooooooooooo'          ");
                  Thread.sleep(100);
                  System.out.println("          'oooooooooooooooc'         'oooooooooooo,          ");
                  Thread.sleep(100);
                  System.out.println("          .ooooooooooooool'   .'     ;lloooooooooo.          ");
                  Thread.sleep(100);
                  System.out.println("           looooooooooooc.   .loc.      looooooool           ");
                  Thread.sleep(100);
                  System.out.println("           ,ooooooooooo,    ;ooooo,    ;ooooooooo,           ");
                  Thread.sleep(100);
                  System.out.println("            :oooooc.      .coooooooc.  .looooooo:            ");
                  Thread.sleep(100);
                  System.out.println("             ;ooooc,'..   ;oooooooooo;  'oooooo;             ");
                  Thread.sleep(100);
                  System.out.println("              .looooooo:.  looooooooooc,looool.              ");
                  System.out.println("                'looooool':ooooooooooooooool'                ");
                  Thread.sleep(100);
                  System.out.println("                  .;loooooooooooooooooool;.                  ");
                  Thread.sleep(100);
                  System.out.println("                     .';clooooooooolc;'.                     ");
                  Thread.sleep(100);
                  System.out.println("                           .......                           \n");

                  System.out.println("----------------------------------------------------------------");
                  System.out.println("-                                                              -");
                  System.out.println("-    Bienvenido al sistema de búsqueda y consulta de vuelos    -");
                  System.out.println("-                                                              -");
                  System.out.println("----------------------------------------------------------------\n");

                  System.out.println("Menú principal");
                  System.out.println("--------------");
                  System.out.println("1. Conocer la información de Países, Aerolíneas y Aeropuertos");
                  System.out.println("2. Consultar precios de tiquetes por trayecto");
                  System.out.println("3. Consultar el estado programado de Llegadas en un aeropuerto");
                  System.out.println("4. Consultar el estado programado de Salidas en un aeropuerto");
                  System.out.println("5. Conocer los vuelos ofrecidos por una aerolínea específica");
                  System.out.println("6. Salir\n");

                  System.out.print("Seleccione una opción: ");
                  int opt = in.nextInt();
                  switch(opt)
                  {
                       case 1:
                         listGeneralInfo();
                         break;
                       case 2:
                         queryPrices();
                         break;
                       case 3:
                         listFlightsAirport(true, null);
                         break;
                       case 4:
                         listFlightsAirport(false, null);
                         break;
                       case 5:
                         listFlightsAirline(null);
                         break;
                       case 6:
                         finish = true;
                         break;
                  }
            
           }
     }


	private void listGeneralInfo()
    {
          in.nextLine();
          boolean finish = false;
          while(!finish)
          {
               Screen.clear();
               System.out.println("Conocer la información de Países, Aerolíneas y Aeropuertos");
               System.out.println("----------------------------------------------------------");
               System.out.println("1. Listar países por código ISO 3166");
               System.out.println("2. Listar Aerolíneas por código IATA");
               System.out.println("3. Listar Aeropuertos por código IATA");
               System.out.println("4. Regresar al menú principal\n");

               System.out.println("Seleccione una opción: ");
               int opt = in.nextInt();

               switch(opt)
               {
                    case 1:
                      String code = getCountryCode();
                      if(code != null)
                      {
                           String name = flightShop.getCountryName(code);
                           String title = String.format("%s - %s\n", code, name);
                           String bar = "";
                           for(int i = 0; i < title.length(); i++)
                           {
                                 bar += "-";
                           }
                           System.out.println(title);
                           System.out.println(bar);
                           System.out.printf("En la actualidad, %s cuenta con %d Aeropuertos que prestan servicios comerciales\n\n", 
                                              name, flightShop.getNumberOfAirportsCountry(code));
                           System.out.println("Presione Enter para continuar...");
                           in.nextLine();
                      }
                      break;
                      case 2:
                         code = getAirlineCode();
                         if(code != null)
                         {
                              boolean stat = printAirlineInfo(code);
                              if(stat)
                              {
                                 System.out.println("\nOpciones adicionales");
                                 System.out.println("--------------------");
                                 System.out.println("1. Consultar vuelos asociados a esta aerolínea");
                                 System.out.println("2. Consultar los aeropuertos en los cuales la aerolínea opera");
                                 System.out.println("3. Regresar al menú anterior\n");

                                 System.out.println("Seleccione una opción: ");
                                 int opt2 = in.nextInt();
                                 switch(opt2)
                                 {
                                       case 1:
                                         listFlightsAirline(code);
                                         break;
                                       case 2:
                                         listAirportsAirline(code);
                                         break;
                                       case 3:
                                         break;
                                 }
                              }
                         }
                         break;
                      case 3:
                         code = getAirportCode();
                         if(code != null)
                         {
                               printAirportInfo(code);
                               System.out.println("\nOpciones adicionales");
                               System.out.println("--------------------");
                               System.out.println("1. Consultar estado programado de llegadas");
                               System.out.println("2. Consultar estado programado de salidas");
                               System.out.println("3. Listar aerolíneas que operan en este aeropuerto");
                               System.out.println("4. Regresar al menú anterior\n");

                               System.out.print("Seleccione una opción: ");
                               int opt2 = in.nextInt();
                               switch(opt2)
                               {
                                    case 1:
                                      listFlightsAirport(true, code);
                                      break;
                                    case 2:
                                      listFlightsAirport(false, code);
                                      break;
                                    case 3:
                                      listAirlinesAirport(code);
                                      break;
                                    case 4:
                                      break;
                               }
                         }
                         break;
                      case 4:
                         finish = true;
                         break;
               }
          }
    }


	private String selectionList(String[] list, int elemByPage, String elem, String plural)
    {

         boolean finish = false;
         int page = 0;
         if(list == null)
         {
              System.out.println("No se han encontrado resultados");
              System.out.print("Presione Enter para continuar...");
              in.nextLine();
              return null;
         }
         int numPages = list.length/elemByPage;
         String code = null;
         int elemPerCol = elemByPage/2;

         while(!finish)
         {
            Screen.clear();
            System.out.println("Selección de "+plural);
            System.out.println("-------------------");
            System.out.printf("%-20s  %-20s\n", "Código", "Nombre");
            for(int i = page*(elemByPage); i < list.length && i < page*(elemByPage)+elemByPage ; i += 1)
            {
                    System.out.println(list[i]);
            }
            System.out.println("\nMostrando página: "+(page + 1)+" de "+(numPages+1));

            System.out.println("1. Ingresar código de "+elem);
            if(page > 0 && page < numPages)
            {
                System.out.println("2. Avanzar página");
                System.out.println("3. Retroceder página");
                System.out.println("4. Regresar al menú anterior\n");
            }
            else if(page == 0)
            {
                System.out.println("2. Avanzar página");
                System.out.println("3. Regresar al menú anterior\n");
            }
            else
            {
                System.out.println("2. Retroceder página");
                System.out.println("3. Regresar al menú anterior\n");
            }
            System.out.print("Seleccione una opción: ");
            int opt = in.nextInt();
            in.nextLine();
            switch(opt)
            {
                  case 1:
                    System.out.print("\nIngrese el código seleccionado: ");
                    code = in.nextLine();
                    finish = true;
                    break;
                  case 2:
                    if(page == numPages)
                    {
                         page--;
                    }
                    else
                    {
                         page++;
                    }
                    break;
                  case 3:
                    if(page == numPages || page == 0)
                    {
                        finish = true;
                    }
                    else
                    {
                        page--;
                    }
                    break;
                  case 4:
                    finish = true;
                    break;
            }
         }
         return code;
    }

    private String getCountryCode()
    {
         
         String[] countryList = flightShop.getCountryList();
         return selectionList(countryList, 13, "país", "países");
    }

    private String getAirlineCode()
    {
         String[] airlineList = flightShop.getAirlineList();
         return selectionList(airlineList, 13, "aerolínea", "aerolíneas");
    }

    private String getAirportCode()
    {
         
         String country = getCountryCode();
         if(country == null)
         {
              return null;
         }
         String[] airportList = flightShop.getAirportList(country);
         return selectionList(airportList, 13, "aeropuerto", "aeropuertos");
    }

    private boolean printAirlineInfo(String code)
    {
         
         Screen.clear();
         boolean stat = false;
         Airline airline = flightShop.getAirline(code);
         if(airline != null)
         {
            System.out.println("Información aerolínea");
            System.out.println("---------------------");
            System.out.println("Nombre: "+airline.name);
            System.out.println("País: "+flightShop.getCountryName(airline.iso_country_code));
            System.out.println("Código IATA: "+airline.iata_code);
            System.out.println("Código ICAO: "+airline.icao_code);
            stat = true;
         }
         else
         {
            System.out.println("No se encontraron resultados para la búsqueda solicitada.");
         }
         System.out.print("\nPresione Enter para continuar...");
         in.nextLine();
         return stat;
    }

    private int getWeekDay()
    {
         
         Screen.clear();
         WeekDays[] days = WeekDays.values();
         System.out.println("Selección día de la semana");
         System.out.println("--------------------------");
         for(WeekDays w : days)
         {
              System.out.println(w);
         }
         System.out.println("8. Regresar al menú anterior\n");

         System.out.print("Seleccione una opción: ");
         int opt = in.nextInt();
         if(opt == 8)
         {
             opt = -1;
         }
         return opt;

     }

    private void listFlightsAirline(String code)
    {
         
         Screen.clear();
         if(code == null)
         {
             code = getAirlineCode();
         }
         if(code != null)
         {
            int day = getWeekDay();
            if(day > 0)
            {
                 ArrayList<Flight> flights = flightShop.getAirlineFlightsDay(code, day);
                 listFlightInfo(flights);
            }
         }
    }

    private void listFlightInfo(ArrayList<Flight> flights)
    {
              String[] representation = new String[flights.size()];
              int i = 0;
              for(Flight f : flights)
              {
                   representation[i] = (i+1)+". "+f.toString();
                   i++;
              }
              String opt = selectionList(representation, 13, "vuelo (número de lista)", "vuelos");
              if(opt != null)
              {
                   int num = Integer.parseInt(opt);
                   Flight f = flights.get(num-1);
                   Airport airport = flightShop.getAirport(f.departure_iata_code);
                   Airport airport2 = flightShop.getAirport(f.arrival_airport_code);
                   Airline airline = flightShop.getAirline(f.airline_iata_code);

                   Screen.clear();
                   System.out.println("Información detallada vuelo");
                   System.out.println("---------------------------");
                   System.out.println("Aerolínea: "+airline.name);
                   System.out.println("Número vuelo: "+f.airline_iata_code+f.flight_number+"\n");

                   System.out.println("Ciudad de salida: "+airport.city+" ("+flightShop.getCountryName(airport.iso_country_code)+")");
                   System.out.println("Aeropuerto salida: "+airport.name+" ("+f.departure_iata_code+")");
                   System.out.println("Hora de salida: "+f.departure_time+" (UTC"+(f.departure_utcoffset.charAt(0) == '-' ? f.departure_utcoffset : "+"+f.departure_utcoffset)+")");
                   System.out.println("Terminal de salida: "+f.departure_airport_terminal+"\n");

                   System.out.println("Ciudad de llegada: "+airport2.city+" ("+flightShop.getCountryName(airport2.iso_country_code)+")");
                   System.out.println("Aeropuerto llegada: "+airport2.name+" ("+f.arrival_airport_code+")");
                   System.out.println("Hora de llegada: "+f.arrival_time+" (UTC"+(f.arrival_utcoffset.charAt(0) == '-' ? f.arrival_utcoffset : "+"+f.arrival_utcoffset)+")");
                   System.out.println("Terminal de llegada: "+f.arrival_airport_terminal+"\n");

                   System.out.println("Aeronave: "+f.aircraft);
                   System.out.println("Tiempo total de vuelo: "+f.elapsed_time+"m\n");

                   System.out.println("Escalas");
                   System.out.println(".......");
                   if(f.getStops().size() > 0)
                   {
                        for(Stop s : f.getStops())
                        {
                             Airport stop = flightShop.getAirport(s.iata_code);
                             System.out.println("Ciudad: "+stop.city+" ("+flightShop.getCountryName(stop.iso_country_code)+")");
                             System.out.println("Aeropuerto: "+stop.name+" ("+stop.iata_code+")");
                             System.out.println("Hora de llegada: "+s.arrival_time+" (UTC"+(s.utcoffset.charAt(0) == '-' ? s.utcoffset : "+"+s.utcoffset)+")");
                             System.out.println("Hora de salida: "+s.departure_time+" (UTC"+(s.utcoffset.charAt(0) == '-' ? s.utcoffset : "+"+s.utcoffset)+")");
                             System.out.println("Tiempo de escala: "+s.stop_duration+"m");
                             System.out.println("Tiempo de vuelo: "+s.elapsed_time+"m\n");
                        }
                   }
                   else
                   {
                         System.out.println("Vuelo directo sin escalas\n");
                   }

                   System.out.println("Precios y equipaje");
                   System.out.println("..................");
                   System.out.println("Tarifa base: "+f.base_fare+" "+f.base_fare_currency);
                   System.out.println("Impuestos: "+f.taxes+" "+f.taxes_currency);
                   System.out.println("Precio total: "+f.total_price+" "+f.total_price_currency+"\n");

                   System.out.println("Número de maletas: "+f.num_bags+"\n");

                   System.out.print("Presione Enter para continuar...");
                   in.nextLine();

              }
    }

    private void listAirportsAirline(String code)
    {
    	 String[] codes = flightShop.getAirportListAirline(code);
    	 String airportCode = selectionList(codes, 13, "aeropuerto", "aeropuertos");
    	 if(airportCode != null)
    	 {
    		  printAirportInfo(airportCode);
    	 }
    }
    
    private void listAirlinesAirport(String code) 
    {

		 String[] info = flightShop.getAirlineListAirport(code);
		 String airlineCode = selectionList(info, 13, "aerolínea", "aerolíneas");
		 if(airlineCode != null)
		 {
			 printAirlineInfo(airlineCode);
		 }
	}

	private void printAirportInfo(String code) 
	{

       Screen.clear();
       Airport air = flightShop.getAirport(code);
       System.out.println("Información detallada del aeropuerto");
       System.out.println("------------------------------------");
       System.out.println("Nombre: "+air.name);
       System.out.println("Ciudad: "+air.city);
       System.out.println("País: "+flightShop.getCountryName(air.iso_country_code));
       System.out.println("Código IATA: "+air.iata_code);
       System.out.println("Código ICAO: "+air.icao_code+"\n");

       String utc = air.utcoffset.charAt(0) == '-' ? air.utcoffset : "+"+air.utcoffset;
       System.out.println("Huso horario: UTC"+utc);
       System.out.println("Latitud: "+air.latitude);
       System.out.println("Longitud: "+air.longitude);
       System.out.println("Altitud: "+air.altitude+"\n");

       System.out.print("Presione Enter para continuar...");
       in.nextLine();
	}
	
	private void listFlightsAirport(boolean arrivals, String code) 
	{

         Screen.clear();
         if(code == null)
         {
              code = getAirportCode();
         }
         if(code != null)
         {
            int day = getWeekDay();

            if(day > 0)
            {
               ArrayList<Flight> flights = null;
               if(arrivals)
               {
                    flights = flightShop.getAirportArrivals(code, day);
               }
               else
               {
                    flights = flightShop.getAirportDepartures(code, day);
               }
               if(flights != null)
               {
                    listFlightInfo(flights);
               }
               else
               {
                    in.nextLine();
                    System.out.println("No se han encontrado resultados\n");
                    System.out.print("Presione Enter para continuar...");
                    in.nextLine();
               }
          }
         }
	}

	private void queryPrices() 
	{
         String depCode = getAirportCode();
         if(depCode != null)
         {
              String arrCode = getAirportCode();
              if(arrCode != null)
              {
                   int day = getWeekDay();
                   if(day > 0)
                   {
                        ArrayList<Flight> flights = flightShop.getFlight(depCode, arrCode, day);
                        if(flights.size() > 0)
                        {
                              listFlightInfo(flights);
                        }
                        else
                        {
                              in.nextLine();
                              System.out.println("No se han encontrado resultados\n");
                              System.out.print("Presione Enter para continuar...");
                              in.nextLine();
                        }
                   }
              }
         }
	}

    private static enum WeekDays
    {
         LUNES(1, "Lunes"),
         MARTES(2, "Martes"),
         MIERCOLES(3, "Miercoles"),
         JUEVES(4, "Jueves"),
         VIERNES(5, "Viernes"),
         SABADO(6, "Sábado"),
         DOMINGO(7, "Domingo");

         private final int iso_weekday;
         private final String name;

         WeekDays(int weekday, String nName)
         {
              iso_weekday = weekday;
              name = nName;
         }

         @Override
         public String toString()
         {
             return String.format("%d. %s", iso_weekday, name);
         }

    }
}
