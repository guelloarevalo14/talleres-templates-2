package taller;

import taller.interfaz.FlightShopCLI;

public class Main
{
  /**
       Punto de entrada de la aplicación.
   **/
  public static void main(String[] args)
  {
        try {
			new FlightShopCLI().mainMenu();
		} catch (Exception e) {
			e.printStackTrace();
		}
  }
  
}
