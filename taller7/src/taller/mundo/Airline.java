package taller.mundo;

/*
 * Airline.java
 * This file is part of FlightShopCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * FlightShop is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * FlightShop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FlightShop. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  La clase <tt>Airline</tt> contiene la información de descripción y
 *  abstracción de una compañía prestadora de servicios de transporte
 *  aéreos. La información presentada, corresponde y cumple con los
 *  criterios dispuestos por la industria aeronáutica, a través de la 
 *  Asociación Internacional de Transporte Aéreo (IATA) y la Organiza-
 *  ción Internacional de Aviación Civil (ICAO).
 *  @author ISIS1206 Team
 */

public class Airline
{
    /**
     * Cadena de texto no modificable que contiene el identificador IATA
     * único asociado a la compañía de aviación.
     **/
    public final String iata_code;

    /**
     * Cadena de texto no modificable que contiene el identificador ICAO
     * único asociado a la compañía de aviación.
     **/
    public final String icao_code;

    /**
     * Cadena de texto no modificable que contiene el código ISO-3166
     * único asociado al país en el cual se encuentra incorporada la
     * aerolínea.
     **/
    public final String iso_country_code;

    /**
     * Cadena de texto no modificable que contiene el nombre (informal)
     * de la aerolínea en cuestión.
     **/
    public final String name;

    /**
     * Constructor principal de la clase, genera una instancia de una Aerolínea.
     *
     * @param iata_code Cadena de texto que contiene el identificador IATA
     * único asignado a la compañía de aviación, para mayor información ver: <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA Codes</a>
     *
     * @param icao_code Cadena de texto que contiene el identificador ICAO
     * único asignado a la compañía de aviación, para mayor información ver: <a href="https://en.wikipedia.org/wiki/Airline_codes#ICAO_airline_designator">ICAO Airline Designator</a>
     *
     * @param iso_country_code Cadena de texto que contiene el identificador ISO-3166
     * asociado al país en el cual se encuentra incorporada la aerolínea, para mayor información ver: <a href="https://en.wikipedia.org/wiki/ISO_3166-2">ISO 3166-2</a>
     *
     * @param name Nombre de la aerolínea.
     **/
    public Airline(String iata_code, String icao_code, String iso_country_code, String name)
    {
         this.iata_code = iata_code;
         this.icao_code = icao_code;
         this.iso_country_code = iso_country_code;
         this.name = name;
    }

    @Override
    public int hashCode()
    {
         return iata_code.hashCode();
    }
}
