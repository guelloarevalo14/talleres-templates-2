package taller.mundo;

/*
 * Flight.java
 * This file is part of FlightShopCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * FlightShop is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * FlightShop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FlightShop. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;

/**
 *  La clase <tt>Flight</tt> contiene la información de descripción y
 *  abstracción de un vuelo comercial. La información presentada, 
 *  corresponde y cumple con los criterios dispuestos por la industria 
 *  aeronáutica, a través de la  Asociación Internacional de Transporte 
 *  Aéreo (IATA) y la Organización Internacional de Aviación Civil (ICAO).
 *  Los precios almacenados (Si existen), corresponden a los precios existentes
 *  durante el mes de Enero del año 2016.
 *  @author ISIS1206 Team
 */

public class Flight
{
     /**
      * Cadena de texto no modificable que contiene el identificador IATA
      * único asociado al aeropuerto de origen del vuelo. Para mayor información,
      * ver: <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA Codes</a>
      **/
     public final String departure_iata_code;

     /**
      * Cadena de texto no modificable que contiene el identificador IATA
      * único asociado al aeropuerto de destino del vuelo. Para mayor información,
      * ver: <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA Codes</a>
      **/
     public final String arrival_airport_code;

     /**
      * Número que contiene el tiempo total de vuelo (Expresado en minutos).
      **/
     public final int elapsed_time;

     /**
      * Cadena de texto que contiene el tiempo estimado de salida del vuelo actual, 
      * expresado en formato HH:mm:ss.
      **/
     public final String departure_time;

     /**
      * Cadena de texto que contiene el tiempo estimado de llegada del vuelo actual, 
      * expresado en formato HH:mm:ss.
      **/
     public final String arrival_time;

     /**
      * Cadena de texto que contiene el nombre del términal de salida al interior
      * del aeropuerto de origen.
      **/
     public final String departure_airport_terminal;

     /**
      * Cadena de texto que contiene el nombre del términal de llegada al interior
      * del aeropuerto de destino.
      **/
     public final String arrival_airport_terminal;

     /**
      * Cadena de texto que contiene el número de vuelo asignado a la ruta por
      * parte de la aerolínea.
      **/
     public final String flight_number;

     /**
      * Cadena de texto no modificable que contiene el identificador IATA
      * único asociado a la compañía de aviación. Para mayor información, ver:
      * <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA Codes</a>
      **/
     public final String airline_iata_code;

     /**
      * Cadena de texto que contiene el Huso horario en el cual se encuentra situado el aeropuerto
      * de origen, conforme a la definición dispuesta por el Tiempo Universal
      * coordinado (UTC). Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/Coordinated_Universal_Time">
      * Coordinated Universal Time</a>
      **/
     public final String departure_utcoffset;

     /**
      * Cadena de texto que contiene el Huso horario en el cual se encuentra situado el aeropuerto
      * de destino, conforme a la definición dispuesta por el Tiempo Universal
      * coordinado (UTC). Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/Coordinated_Universal_Time">
      * Coordinated Universal Time</a>
      **/
     public final String arrival_utcoffset;

     /**
      * Número real que contiene la tarifa base de un boleto en el trayecto actual (En clase económica).
      **/
     public final double base_fare;

     /**
      * Cadena de texto que contiene el identificador ISO-4216 asociado a la moneda de cobro
      * de la tarifa base. Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/ISO_4217">ISO 4217</a>
      **/
     public final String base_fare_currency;

     /**
      * Número real que contiene los impuestos asociados al boleto actual.
      **/
     public final double taxes;

     /**
      * Cadena de texto que contiene el identificador ISO-4216 asociado a la moneda de cobro
      * de los impuestos asociados al boleto de vuelo. Para mayor información, ver: 
      * <a href="https://en.wikipedia.org/wiki/ISO_4217">ISO 4217</a>
      **/
     public final String taxes_currency;

     /**
      * Número real que contiene el precio total de un boleto (En clase económica)
      * en el vuelo actual.
      **/
     public final double total_price;

     /**
      * Cadena de texto que contiene el identificador ISO-4216 asociado a la moneda de cobro
      * de los impuestos asociados al boleto de vuelo. Para mayor información, ver: 
      * <a href="https://en.wikipedia.org/wiki/ISO_4217">ISO 4217</a>
      **/
     public final String total_price_currency;

     /**
      * Número entero que contiene el número máximo de maletas que se permiten a bordo
      * de la cabina de la aeronave, sin incurrir en costos adicionales.
      **/
     public final int num_bags;

     /**
      * Cadena de texto que contiene el identificador IATA único asociado a la aeronave dispuesta
      * para el vuelo actual. Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/List_of_ICAO_aircraft_type_designators">ICAO and IATA Aircraft Designators</a>
      **/
     public final String aircraft;

     /**
      * Lista que contiene las escalas del presente vuelo. {@see Stop}
      **/
     private ArrayList<Stop> stops;

     /**
      * Día de operación de la ruta, donde 1 corresponde al
      * día Lunes, y 7, corresponde al día Domingo.
      **/
     public final int departure_day; 

     /**
      * Día de llegada de la ruta, donde 1 corresponde al
      * día Lunes, y 7, corresponde al día Domingo.
      **/
     public final int arrival_day;
     
     /**
      * Constructor principal de la clase, genera una instancia de un vuelo.
      *
      * @param departure_iata_code Cadena de texto no modificable que contiene el identificador IATA
      * único asociado al aeropuerto de origen del vuelo
      *
      * @param arrival_airport_code Cadena de texto no modificable que contiene el identificador IATA
      * único asociado al aeropuerto de destino del vuelo
      *
      * @param elapsed_time Número que contiene el tiempo total de vuelo (Expresado en minutos). elapsed_time > 0
      *
      * @param departure_day Número entero que contiene el día de operación de la ruta, donde 1 corresponde al 
      * día Lunes, y 7, corresponde al día Domingo.
      *
      * @param arrival_day Número entero que contiene el día de llegada de la ruta, donde 1 corresponde al 
      * día Lunes, y 7, corresponde al día Domingo.
      *
      * @param departure_time Cadena de texto que contiene el tiempo estimado de salida del vuelo actual, 
      * expresado en formato HH:mm:ss
      *
      * @param arrival_time Cadena de texto que contiene el tiempo estimado de llegada del vuelo actual, 
      * expresado en formato HH:mm:ss
      *
      * @param departure_airport_terminal Cadena de texto que contiene el nombre del términal de salida al interior
      * del aeropuerto de origen.
      *
      * @param arrival_airport_terminal Cadena de texto que contiene el nombre del términal de llegada al interior
      * del aeropuerto de destino
      *
      * @param flight_number Cadena de texto que contiene el número de vuelo asignado a la ruta por
      * parte de la aerolínea
      *
      * @param airline_iata_code Cadena de texto no modificable que contiene el identificador IATA
      * único asociado a la compañía de aviación
      *
      * @param departure_utcoffset Cadena de texto que contiene el Huso horario en el cual se encuentra situado el aeropuerto
      * de origen, conforme a la definición dispuesta por el Tiempo Universal
      * coordinado (UTC)
      *
      * @param arrival_utcoffset Cadena de texto que contiene el Huso horario en el cual se encuentra situado el aeropuerto
      * de destino, conforme a la definición dispuesta por el Tiempo Universal
      * coordinado (UTC)
      *
      * @param base_fare Número real que contiene la tarifa base de un boleto en el trayecto actual (En clase económica)
      *
      * @param base_fare_currency Cadena de texto que contiene el identificador ISO-4216 asociado a la moneda de cobro
      * de la tarifa base
      *
      * @param taxes Número real que contiene los impuestos asociados al boleto actual
      *
      * @param taxes_currency Cadena de texto que contiene el identificador ISO-4216 asociado a la moneda de cobro
      * de los impuestos asociados al boleto de vuelo
      *
      * @param total_price Número real que contiene el precio total de un boleto (En clase económica)
      * en el vuelo actual
      *
      * @param total_price_currency Cadena de texto que contiene el identificador ISO-4216 asociado a la moneda de cobro
      * de los impuestos asociados al boleto de vuelo
      *
      * @param num_bags Número entero que contiene el número máximo de maletas que se permiten a bordo
      * de la cabina de la aeronave, sin incurrir en costos adicionales
      *
      * @param aircraft Cadena de texto que contiene el identificador IATA único asociado a la aeronave dispuesta
      * para el vuelo actual
      **/
     public Flight(String departure_iata_code, String arrival_airport_code, int elapsed_time, int departure_day, int arrival_day, 
                   String departure_time, String arrival_time, String departure_airport_terminal, 
                   String arrival_airport_terminal, String flight_number, String airline_iata_code, 
                   String departure_utcoffset, String arrival_utcoffset, double base_fare, 
                   String base_fare_currency, double taxes, String taxes_currency,
                   double total_price, String total_price_currency,
                   int num_bags, String aircraft)
     {
            this.stops = new ArrayList<Stop>();
            this.arrival_day = arrival_day;
            this.departure_day = departure_day;
            this.departure_iata_code = departure_iata_code; 
            this.arrival_airport_code = arrival_airport_code; 
            this.elapsed_time = elapsed_time; 
            this.departure_time = departure_time; 
            this.arrival_time = arrival_time; 
            this.departure_airport_terminal = departure_airport_terminal; 
            this.arrival_airport_terminal = arrival_airport_terminal; 
            this.flight_number = flight_number; 
            this.airline_iata_code = airline_iata_code; 
            this.departure_utcoffset = departure_utcoffset; 
            this.arrival_utcoffset = arrival_utcoffset; 
            this.base_fare = base_fare; 
            this.base_fare_currency = base_fare_currency; 
            this.taxes = taxes; 
            this.taxes_currency = taxes_currency;
            this.total_price = total_price;
            this.total_price_currency = total_price_currency;
            this.num_bags = num_bags; 
            this.aircraft = aircraft; 
     }

     /**
      * Añade una escala a la ruta actual.
      * @param stop Descripción de la escala a realizar.
      **/
     public void addStop(Stop stop)
     {
          stops.add(stop);
     }

     /**
      * Retorna una lista de las escalas realizadas por el presente vuelo.
      * @return Una lista que contiene las escalas realizadas por el vuelo
      **/
     public ArrayList<Stop> getStops()
     {
          return stops;
     }

     @Override
     public String toString()
     {
          return airline_iata_code+flight_number+" - "+departure_time+" : "+departure_iata_code+" → "+arrival_airport_code+" "+total_price+total_price_currency;
     }

}
