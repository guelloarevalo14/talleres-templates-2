package taller.mundo;

/*
 * Airport.java
 * This file is part of FlightShopCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * FlightShop is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * FlightShop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FlightShop. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  La clase <tt>Airport</tt> contiene la información de descripción y
 *  abstracción de un aeropuerto comercial. La información presentada, 
 *  corresponde y cumple con los criterios dispuestos por la industria 
 *  aeronáutica, a través de la  Asociación Internacional de Transporte 
 *  Aéreo (IATA) y la Organización Internacional de Aviación Civil (ICAO).
 *  @author ISIS1206 Team
 */

public class Airport
{
    /**
     * Cadena de texto no modificable que contiene el identificador IATA
     * único asociado al aeropuerto. Para mayor información, ver:
     * <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA Codes</a>
     **/
    public final String iata_code;

    /**
     * Cadena de texto no modificable que contiene el identificador ICAO
     * único asociado al aeropuerto. Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/International_Civil_Aviation_Organization_airport_code">ICAO Airport Codes</a>
     * 
     **/
    public final String icao_code;

    /**
     * Cadena de texto no modificable que contiene el código ISO-3166
     * único asociado al país en el cual se encuentra ubicado el aero-
     * puerto. Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/ISO_3166-2">ISO 3166-2</a>
     **/
    public final String iso_country_code;

    /**
     * Cadena de texto que contiene el nombre de la ciudad en la cual se
     * encuentra el aeropuerto.
     **/
    public final String city;

    /**
     * Nombre de la zona temporal asignada a la ubicación del aeropuerto
     * por parte de la ICANN. Para mayor información, ver: <a href = "https://en.wikipedia.org/wiki/Tz_database">
     * tz database</a>
     **/
    public final String tz_name;

    /**
     * Huso horario en el cual se encuentra situado el aeropuerto,
     * conforme a la definición dispuesta por el Tiempo Universal
     * coordinado (UTC). Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/Coordinated_Universal_Time">
     * Coordinated Universal Time</a>
     **/
    public final String utcoffset;

    /**
     * Altitud (Expresada en metros) a la cual se encuentra el aeropuerto
     * actual.
     **/
    public final String altitude;

    /**
     * Nombre del aeropuerto actual.
     **/
    public final String name;

    /**
     * Nombre completo del aeropuerto actual (Incluyendo nombre de país y ciudad).
     **/
    public final String full_name;

    /**
     * Latitud geográfica del aeropuerto en el mapa real.
     **/
    public final String latitude;

    /**
     * Longitud geográfica del aeropuerto en el mapa real.
     **/
    public final String longitude;

    /**
     * Constructor principal de la clase, genera una instancia de un 
     * Aeropuerto.
     *
     * @param iata_code Cadena de texto no modificable que contiene el identificador IATA
     * único asociado al aeropuerto
     *
     * @param icao_code Cadena de texto no modificable que contiene el identificador ICAO
     * único asociado al aeropuerto
     *
     * @param iso_country_code Cadena de texto no modificable que contiene el código ISO-3166
     * único asociado al país en el cual se encuentra ubicado el aeropuerto
     *
     * @param city Cadena de texto que contiene el nombre de la ciudad en la cual se
     * encuentra el aeropuerto
     *
     * @param tz_name Nombre de la zona temporal asignada a la ubicación del aeropuerto
     * por parte de la ICANN
     *
     * @param utcoffset Huso horario en el cual se encuentra situado el aeropuerto,
     * conforme a la definición dispuesta por el Tiempo Universal
     * coordinado (UTC)
     *
     * @param altitude Altitud (Expresada en metros) a la cual se encuentra el aeropuerto
     * actual
     *
     * @param name Nombre del aeropuerto actual
     *
     * @param full_name Nombre completo del aeropuerto actual (Incluyendo nombre de país y ciudad)
     *
     * @param latitude Latitud geográfica del aeropuerto en el mapa real
     *
     * @param longitude Longitud geográfica del aeropuerto en el mapa real
     **/
    public Airport(String iata_code, String icao_code, String iso_country_code, 
                   String city, String tz_name, String utcoffset,
                   String altitude, String name, String full_name,
                   String latitude, String longitude)
    {
          this.iata_code = iata_code;
          this.icao_code = icao_code;
          this.iso_country_code = iso_country_code;
          this.city = city;
          this.tz_name = tz_name;
          this.utcoffset = utcoffset;
          this.altitude = altitude;
          this.name = name;
          this.full_name = full_name;
          this.latitude = latitude;
          this.longitude = longitude;
    }

    @Override
    public int hashCode()
    {
        return icao_code.hashCode();
    }

}
