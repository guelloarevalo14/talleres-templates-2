package taller.mundo;

/*
 * Stop.java
 * This file is part of FlightShopCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * FlightShop is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * FlightShop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FlightShop. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  La clase <tt>Stop</tt> contiene la información de descripción y
 *  abstracción de una escala realizada por un vuelo comercial. 
 *  La información presentada, corresponde y cumple con los criterios 
 *  dispuestos por la industria aeronáutica, a través de la  Asociación 
 *  Internacional de Transporte Aéreo (IATA) y la Organización 
 *  Internacional de Aviación Civil (ICAO).
 *  @author ISIS1206 Team
 **/

public class Stop
{
    /**
     * Cadena de texto no modificable que contiene el identificador IATA
     * único asociado al aeropuerto en el cual se realiza la escala. Para mayor información, ver:
     * <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA Codes</a>
     **/
    public final String iata_code;

    /**
     * Cadena de texto que contiene el tiempo estimado de inicio de la escala actual,
     * expresado en formato HH:mm:ss.
     **/
    public final String arrival_time;

    /**
     * Cadena de texto que contiene el tiempo estimado de fin de la escala actual, 
     * expresado en formato HH:mm:ss.
     **/
    public final String departure_time;

    /**
     * Número que contiene el tiempo parcial de vuelo (Expresado en minutos).
     **/
    public final int elapsed_time;

    /**
     * Número que contiene el tiempo total de escala (Expresado en minutos).
     **/
    public final int stop_duration;

    /**
     * Cadena de texto que contiene el Huso horario en el cual se encuentra situado el aeropuerto
     * en el cual se realiza la escala, conforme a la definición dispuesta por el Tiempo Universal
     * coordinado (UTC). Para mayor información, ver: <a href="https://en.wikipedia.org/wiki/Coordinated_Universal_Time">
     * Coordinated Universal Time</a>
     **/
    public final String utcoffset;

    /**
     * Constructor principal de la clase, genera una instancia de una Escala
     *
     * @param iata_code Cadena de texto no modificable que contiene el identificador IATA
     * único asociado al aeropuerto en el cual se realiza la escala
     *
     * @param arrival_time Cadena de texto que contiene el tiempo estimado de inicio de la escala actual,
     * expresado en formato HH:mm:ss
     *
     * @param departure_time Cadena de texto que contiene el tiempo estimado de fin de la escala actual, 
     * expresado en formato HH:mm:ss
     *
     * @param elapsed_time Número que contiene el tiempo parcial de vuelo (Expresado en minutos)
     *
     * @param stop_duration Número que contiene el tiempo total de escala (Expresado en minutos)
     *
     * @param utcoffset Cadena de texto que contiene el Huso horario en el cual se encuentra situado el aeropuerto
     * en el cual se realiza la escala, conforme a la definición dispuesta por el Tiempo Universal
     * coordinado (UTC)
     **/
    public Stop(String iata_code, String arrival_time, String departure_time,
                int elapsed_time, int stop_duration, String utcoffset)
    {
         this.iata_code = iata_code;
         this.arrival_time = arrival_time;
         this.departure_time = departure_time;
         this.elapsed_time = elapsed_time;
         this.stop_duration = stop_duration;
         this.utcoffset = utcoffset;
    }

}
