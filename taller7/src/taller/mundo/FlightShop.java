package taller.mundo;

/*
 * FlightShop.java
 * This file is part of FlightShopCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * FlightShop is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * FlightShop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FlightShop. If not, see <http:
 */

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

/**
 *  La clase <tt>FlightShop</tt> abstrae y simula el funcionamiento de un
 *  sistema de información y reservas de boletos de vuelo en línea,
 *  implementa operaciones de consulta y recuperación de información
 *  acerca de Aerolíneas, Aeropuertos y Vuelos. La notación y nomenclatura
 *  usada, corresponde y cumple con los criterios dispuestos por la industria 
 *  aeronáutica, a través de la  Asociación  Internacional de Transporte Aéreo 
 *  (IATA) y la Organización Internacional de Aviación Civil (ICAO).
 *  @author ISIS1206 Team
 **/

public class FlightShop
{

      /**
       * Constante que establece la ubicación del conjunto de datos comprimido
       **/
      private static final String COMPRESSED_USER_DATA = "./data/flight_data.tar.bz2";

      /**
       * Constante que establece la ubicación del archivo de descripción de Países
       * por código ISO-3166
       **/
      private static final String ISO_3166_FILE = "./data/countries.csv";

      /**
       * Constante que establece la ubicación del archivo de descripción de Aerolíneas
       **/
      private static final String AIRLINES_FILE = "./data/airlines.csv";

      /**
       * Constante que establece la ubicación del archivo de descripción de Aeropuertos
       **/
      private static final String AIRPORTS_FILE = "./data/airports.csv";

      /**
       * Constante que establece la ubicación del archivo de descripción de Aerolíneas
       **/
      private static final String FLIGHTS_FILE = "./data/flights.csv";

      /**
       * Constante que establece la ubicación del archivo de descripción de las Escalas
       * de un vuelo
       **/
      private static final String FLIGHT_STOPS_FLIE = "./data/flight_stops.csv";

      /**
          TODO: Definir las estructuras principales a usar durante el funcionamiento del sistema,
                i.e., Diccionarios. Es posible hacer uso de la implementación de referencia disponible
                en la librería estándar de Java (HashMap). O una implementación propia, si se desea.
       **/

      /**
       * Constructor principal de la clase, éste se encuentra encargado
       * de cargar la información de descripción y funcionamiento del sistema
       * de consulta de vuelos, aerolíneas y aeropuertos.
       * @throws FileNotFoundException Si alguno de los archivos que comprenden
       * el conjunto de datos no existe.
       * @throws IOException Si ocurre un error durante el proceso de lectura
       **/
      public FlightShop() throws FileNotFoundException, IOException
      {
           /**
              TODO: Inicialice los parámetros y estructuras de la clase,
                    a continuación, realizar la carga de los archivos
                    disponibles en el directorio data.
            **/
      }

      /**
       * A partir del identificador ISO-3166 asignado a un país, retorna el nombre del mismo.
       * @param code Código ISO-3166 único asociado al país del cual se desea recuperar el
       * nombre. 
       * <p>
       * <b>Para obtener mayor información:</b>
       * Consultar la definición y lista de códigos <a href="https:
       * @return Nombre del país, cuyo código ISO-3166 corresponde al código ingresado.
       **/
      public String getCountryName(String code)
      {
            /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
      }

      /**
       * A partir del identificador IATA designado a una aerolínea comercial, recupera y
       * retorna el objeto de descripción de la misma.
       * @param code Identificador IATA único asignado a la compañía de aviación
       * <p>
       * <b>Para obtener mayor información:</b>
       * Consultar la definición y lista de códigos <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA</a>
       * @return Objeto de descripción de la aerolínea solicitada.
       * @see Airline
       **/
      public Airline getAirline(String code)
      {
            /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
      }

      /**
       * A partir del identificador IATA designado a un aeropuerto que presta servicios
       * comerciales, recupera y retorna el objeto de descripción del mismo.
       * @param code Identificador IATA único asignado al aeropuerto solicitado
       * <p>
       * <b>Para obtener mayor información:</b>
       * Consultar la definición y lista de códigos <a href="https://en.wikipedia.org/wiki/International_Air_Transport_Association_code">IATA</a>
       * @return Objeto de descripción del aeropuerto solicitado.
       * @see Airport
       **/
      public Airport getAirport(String code)
      {
            /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
      }

      /**
       * Recupera la lista de vuelos programados por parte de una aerolínea
       * en un día específico.
       * @param code Identificador IATA único asignado a la compañía de aviación.
       * @param day Día de la semana en la cual se desea realizar la consulta, dónde 1
       * corresponde al día Lunes y 7, al día Domingo
       * @param Una lista que contiene los objetos de descripción de vuelos asociados
       * a la aerolínea solicitada en un día específico.
       * @see Flight
       **/
      public ArrayList<Flight> getAirlineFlightsDay(String code, int day)
      {
            /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
      }
      
      /**
       * Retorna el número de aeropuertos que prestan servicios de aviación
       * comerciales en un país específico (Identificado con código ISO-3166)
       * @param code Código ISO-3166 único asociado al país del cual se desea recuperar el
       * número de aeropuertos
       * @return Número de aeropuertos que prestan servicios de aviación
       * comerciales en el país solicitado
       **/
      public int getNumberOfAirportsCountry(String code) 
      {
           /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return -1;
      }

      /**
       * Retorna un arreglo de cadenas de texto que describen todas las aerolíneas del sistema
       * de acuerdo al código IATA y el nombre asociados.
       * @return Arreglo de cadenas de la forma "<IATA> <Espacio en blanco> <Nombre>" que
       * corresponden a todas las aerolíneas del sistema.
       **/
      public String[] getAirlineList() 
      {
    	  /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
  	  }
      
      /**
       * Retorna un arreglo de cadenas de texto que describen los aeropuertos presentes
       * en un país determinado.
       * @param Código ISO-3166 único asociado al país del cual se desea recuperar los
       * registros
       * @return Arreglo de cadenas de la forma "<IATA> <Espacio en blanco> <Nombre Completo>" que
       * corresponden a todos los aeropuertos presentes en el país solicitado
       **/
      public String[] getAirportList(String country) 
      {
  		       /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
  	  }

      /**
       * Retorna un arreglo de cadenas de texto que describen a todos los países registrados en
       * el sistema.
       * @return Arreglo de cadenas de la forma "<ISO-3166> <Espacio en blanco> <Nombre>" que corresponden
       * a todos los países registrados en el sistema
       **/
      public String[] getCountryList() 
      {
   		   /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
   	  }
      
      /**
       * Recupera y retorna un arreglo de cadenas de texto que describen
       * a los aeropuertos en los cuales opera una aerolínea específica.
       * @param code Identificador IATA único asignado a la aerolínea solicitada
       * @return Un arreglo de cadenas de texto de la forma "<IATA> <Espacio en blanco> <Nombre>" que
       * comprenden a todas los aeropuertos en los cuales opera una aerolínea específica
       **/
      public String[] getAirportListAirline(String code) 
      {
     		   /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
  	  }

      /**
       * Recupera y retorna un arreglo de cadenas de texto que describen
       * a las aerolíneas que operan en un aeropuerto específico.
       * @param code Identificador IATA único asignado al aeropuerto solicitado
       * @return Un arreglo de cadenas de texto de la forma "<IATA> <Espacio en blanco> <Nombre>" que
       * comprenden a todas las aerolíneas que operan en un aeropuerto específico
       **/
      public String[] getAirlineListAirport(String code) 
      {
  		      /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
  	  }

      /**
       * Recupera y retorna una lista de Objetos que describen
       * los vuelos que departen desde un aeropuerto específico en un día específico.
       * @param code Identificador IATA único asignado al aeropuerto solicitado
       * @param day Número del día de la semana a consultar, dónde 1 corresponde al día
       * lunes, y 7, al día Domingo.
       * @return Lista de objetos que describen los vuelos que departen desde un aeropuerto 
       * durante un día de la semana.
       **/
     public ArrayList<Flight> getAirportDepartures(String code, int day)
     {
            /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
     }

     /**
      * Recupera y retorna una lista de Objetos que describen
      * los vuelos que arriban en un aeropuerto específico en un día específico.
      * @param code Identificador IATA único asignado al aeropuerto solicitado
      * @param day Número del día de la semana a consultar, dónde 1 corresponde al día
      * lunes, y 7, al día Domingo.
      * @return Lista de objetos que describen los vuelos que arriban en un aeropuerto 
      * durante un día de la semana.
      **/
     public ArrayList<Flight> getAirportArrivals(String code, int day)
     {
            /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
     }
     
     /**
      * Recupera y retorna la lista de vuelos directos entre un par de aeropuertos
      * durante un día específico de la semana
      * @param depCode Identificador IATA único asociado al aeropuerto de origen del vuelo
      * @param arrCode identificador IATA único asociado al aeropuerto de destino del vuelo
      * @param day Número del día de la semana a consultar, dónde 1 corresponde al día
      * lunes, y 7, al día Domingo.
      * @return Lista de objetos que describen los vuelos directos entre un par de aeropuertos
      * durante un día de la semana
      */
     public ArrayList<Flight> getFlight(String depCode, String arrCode, int day) 
     {
 		      /**
                TODO: Complete la función, conforme a la documentación suministrada.
             **/
            return null;
 	}

    /**
     *  Rutina que permite realizar la descompresión de un contenedor tar.bz2. Durante el
     *  proceso de descompresión, la rutina informa al usuario en consola, además 
     *  del nombre del archivo actual, el progreso de la operación. La rutina de-
     *  tiene la ejecución del programa si ocurre algún error.
     *  @param path La ubicación del archivo tar.bz2 a ser descomprimido.
     **/
    private void uncompressTarBzip(String path)
    {
       String anim= "[|]/-\\";
       int buffersize = 1024;
       try(TarArchiveInputStream is = new TarArchiveInputStream(new BZip2CompressorInputStream(new FileInputStream(path))))
       {
           TarArchiveEntry entry;
           while((entry = is.getNextTarEntry()) != null)
           {
                final byte[] buffer = new byte[buffersize];
                FileOutputStream fout = new FileOutputStream("./data/"+entry.getName());
                
                int size = 0;
                int n = 0;
                long total = entry.getSize();
                while (-1 != (n = is.read(buffer))) 
                {
                    double x = ((size*1.0)/total)*100.0;
                    String progress = "\r" + anim.charAt(((int) Math.round(x)) % anim.length()) + " " + Math.round(x) + "% " + String.format("(%d / %d)", size, total) ;
                    System.out.write(progress.getBytes());
                    size += n;
                    fout.write(buffer, 0, n);
                }
                
                fout.close();
           }
       }
       catch(Exception e)
       {
          e.printStackTrace();
       }
    }

}

